create table app_user
(
    id       bigserial not null,
    name     varchar(255),
    password varchar(255),
    primary key (id)
);

create table message
(
    id      bigserial not null,
    message_text varchar(255),
    user_id int8,
    primary key (id)
);

alter table message
    add constraint message_user_fk
        foreign key (user_id)
            references app_user;
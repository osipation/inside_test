insert into app_user (id, name, password)
values (1, 'test', '$2y$10$Rww/jLAoS2/aaiwdGraulOmtj/InSZy5hyt4MKzhgiAAWwbT4cqmW');
--пароль в зашифрованном виде. "password":"777"

insert into app_user (id, name, password)
values (2, 'fake', '$2y$10$9syiuat4VDdnZxeFFTXF3e0g5BM/LGdywFxGBEz2nEQ3QMSsoRzKK');
--пароль в зашифрованном виде. "password":"888"
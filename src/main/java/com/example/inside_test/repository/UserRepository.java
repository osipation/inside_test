package com.example.inside_test.repository;

import com.example.inside_test.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AppUser, Long> {
    AppUser findByName(String name);
}
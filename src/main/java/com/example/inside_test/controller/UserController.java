package com.example.inside_test.controller;

import com.example.inside_test.entity.Message;
import com.example.inside_test.service.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {
    public static final String HISTORY = "history\\s\\d*";
    private final UserService userService;

    /*Как в ТЗ сделал один эндпоинт*/
    @PostMapping("/user/message")
    public ResponseEntity<List<Message>> addMessageToUser(@RequestBody UserMessageContext userMessageContext) {
        String message_text = userMessageContext.getMessage();
        String userName = userMessageContext.getName();

        if (message_text.matches(HISTORY)) {
            return ResponseEntity.ok().body(userService.getMessages(message_text, userName));
        }
        Message message = new Message(message_text);
        userService.addMessageToUser(message, userName);
        return ResponseEntity.ok().body(List.of(message));
    }
}

@Data
class UserMessageContext {
    private String name;
    private String message;
}
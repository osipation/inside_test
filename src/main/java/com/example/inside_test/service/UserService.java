package com.example.inside_test.service;

import com.example.inside_test.entity.AppUser;
import com.example.inside_test.entity.Message;

import java.util.List;

public interface UserService {
    AppUser getUser(String userName);

    List<Message> getMessages(String message, String userName);

    void addMessageToUser(Message message, String userName);
}
package com.example.inside_test.service;

import com.example.inside_test.entity.AppUser;
import com.example.inside_test.entity.Message;
import com.example.inside_test.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/*Вся логика прописана в UserService */

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    /*данный метод достаёт из БД список сообщений от конкретного пользователя*/
    @Override
    public List<Message> getMessages(String message, String userName) {
        int count = Integer.parseInt(message.substring(8));
        if (count <= 0) {
            return Collections.emptyList();
        }
        AppUser user = getUser(userName);
        List<Message> messages = user.getMessages();
        messages.sort(Comparator.comparing(Message::getId).reversed());
        return messages.stream().limit(count).collect(Collectors.toList());
    }

    /*данный метод добавляет пользователю сообщение в список и одновременно записывает в БД в таблицу "message"*/
    @Override
    public void addMessageToUser(Message message, String userName) {
        userRepository.findByName(userName).getMessages().add(message);
    }


    @Override
    public AppUser getUser(String userName) {
        return userRepository.findByName(userName);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = userRepository.findByName(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found in DB");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        return new User(user.getName(), user.getPassword(), authorities);
    }
}
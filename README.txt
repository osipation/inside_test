Данное приложение позволяет записывать сообщения в базу данных и смотреть историю сообщений.
Функционал доступен только после аутентификации
Инструкция для проверки работоспособности:
-Создаем базу данных и прописываем в application.properties данные datasource.
-Запускаем InsideTestApplication.main();
-C помощью flyway создаются 2 таблицы "app_user" и "message" со связью "one to many" и добавляются 2 пользователя
{"name":"test","password":"777"} и {"name":"fake","password":"888"}
-Приложение стартует и вертится на "http://localhost:8888" (порт можно поменять в application.properties)
-Все curl написаны под cmd.exe
-Аутентификация (Сделана с помощью Spring Security)
    curl -d "{\"name\":\"test\",\"password\":\"777\"}" -H "Content-Type:application/json" -X POST "http://localhost:8888/api/login"
при удачном результате вернется json со сгенерированным access_token
    {"token":"access_token"}
-для проверки функционала
    ~в хэдере Authorization после "Bearer_" вставляем access_token, который вернулся после аутентификации
    ~в body в json при попытке передать "message" он записывается в БД под определенным пользователем ("name")
    ~при попытке отправить сообщение типа "history\s\d*" возвращается список сообщений от этого пользователя, в количестве указанном в сообщении
    ОТПРАВКА СООБЩЕНИЯ
    curl -H "Authorization:Bearer_accessToken" -H "Content-Type:application/json" -d "{\"name\":\"test\",\"message\":\"hi\"}" http://localhost:8888/api/user/message
    ПРОСМОТР ИСТОРИИ
    curl -H "Authorization:Bearer_accessToken" -H "Content-Type:application/json" -d "{\"name\":\"test\",\"message\":\"history 2\"}" http://localhost:8888/api/user/message
